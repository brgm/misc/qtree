# -*- coding: utf-8 -*-
from __future__ import annotations

from collections import defaultdict
from typing import Any, List

from qtpy.QtCore import (
    QAbstractItemModel,
    QByteArray,
    QMimeData,
    QModelIndex,
    QObject,
    Qt,
)

from .qtreeitem import qTreeItem


class qTreeModel(QAbstractItemModel):
    """QTreeView initialized with a qTreeModel.

    Defines drag'n'drop default behavior to internal move.

    Args:
        parent (QObject, optional): parent QObject. Defaults to None.
        columns (List[str], optional): list of `item.data()` attributes to expose. Defaults to [].
    """

    def __init__(self, parent: QObject = None, columns: List[str] = []):

        super().__init__()
        self.root = qTreeItem("__root__", droppable=True)
        assert not self.root.expose_properties
        self.columns = columns or []
        # FIXME: no effect
        for i, name in enumerate(columns):
            self.setHeaderData(i, Qt.Horizontal, name or "#")

    def has_mapped_properties(self, row=None, parent=None):
        if row:
            return self._child_item(row, parent).expose_properties
        return len(self.columns) != 0

    def property_name(self, index: QModelIndex = QModelIndex()):
        assert self.has_mapped_properties()
        return self.columns[index.column()]

    @property
    def length(self):  # WARNING: do NOT implement __len__ !
        """number of children in root"""
        return self.root.length

    def size(self, parent: QModelIndex = None):
        """total number of recursive items in a parent index"""
        if parent and parent.isValid():
            root = parent.internalPointer()
        else:
            root = self.root

        return root.size

    def iterator(self, parent: QModelIndex = None):
        """recursive iterator over a parent index"""
        if parent and parent.isValid():
            root = parent.internalPointer()
        else:
            root = self.root

        yield from root.iterator()

    # overload QAbstractItemModel.rowCount()
    def rowCount(self, parent: QModelIndex = QModelIndex()) -> int:
        """Returns the number of rows under the given parent. When the parent is valid it means that rowCount is returning the number of children of parent."""
        return self._item(parent, self.root).length

    # overload QAbstractItemModel.columnCount()
    def columnCount(self, parent: QModelIndex = None) -> int:
        """Returns the number of columns for the children of the given parent."""
        if parent.isValid():
            if self.has_mapped_properties():
                return len(self.columns)
            return 1
        return 1

    # overload QAbstractItemModel.flags()
    def flags(self, index: QModelIndex) -> Qt.ItemFlag:
        """Returns the item flags for the given index."""
        if not index.isValid():
            return Qt.ItemFlag.NoItemFlags
        item = index.internalPointer()  # or self.root
        assert item is not None and item != self.root
        if self.has_mapped_properties():
            column_value = item.get_property(self.property_name(index))
            if isinstance(column_value, bool):
                return item._flags | Qt.ItemFlag.ItemIsUserCheckable
        return item._flags

    # FIXME: more readable with parent = QModelIndex() ?
    def _item(self, index: QModelIndex = QModelIndex(), default=None):
        # FIXME: assert index is not None
        if index is None:
            return default
        if index.isValid():
            return index.internalPointer()
        return default

    def _child_item(self, row: int, parent: QModelIndex = QModelIndex()):
        # all columns point to the same item
        if row < 0:
            return None
        return self._item(parent, self.root).child(row)

    # overload QAbstractItemModel.index()
    def index(
        self, row: int, column: int = 0, parent: QModelIndex = QModelIndex()
    ) -> QModelIndex:
        """Returns the index of the item in the model specified by the given row, column and parent index."""
        if item := self._child_item(row, parent):
            return self.createIndex(row, column, item)
        return QModelIndex()

    # overload QAbstractItemModel.parent()
    def parent(self, child: QModelIndex = QModelIndex()) -> QModelIndex:
        """Returns the parent of the model item with the given index. If the item has no parent, an invalid QModelIndex is returned."""
        if item := self._item(child):
            if item.parent == self.root:
                return QModelIndex()
            return self.createIndex(item.parent.row_in_parent(), 0, item.parent)
        return QModelIndex()

    # overload QAbstractItemModel.data()
    def data(self, index: QModelIndex, role: int = Qt.DisplayRole) -> Any:
        """Returns the data stored under the given role for the item referred to by the index."""
        if role not in (Qt.DisplayRole, Qt.CheckStateRole, Qt.EditRole):
            return None

        value = None

        item = index.internalPointer()
        assert isinstance(item, qTreeItem)

        if item.expose_properties and self.has_mapped_properties():
            value = item.get_property(self.property_name(index))
        elif index.column() == 0:
            value = item.content

        if role == Qt.CheckStateRole:
            if not isinstance(value, (bool, Qt.CheckState)):
                return None
            elif isinstance(value, bool):
                value = Qt.CheckState.Checked if value else Qt.CheckState.Unchecked

        return value

    # overload QAbstractItemModel.setData()
    def setData(
        self,
        index: QModelIndex,
        value: Any,
        role: int = Qt.EditRole,
    ) -> bool:
        """Sets the role data for the item at index to value. Returns true if successful; otherwise returns false."""
        if role not in (Qt.CheckStateRole, Qt.EditRole):
            return False

        if not index.isValid():
            return False

        item = index.internalPointer()

        if item is None:
            return False

        if role == Qt.CheckStateRole and isinstance(self.data(index, role), bool):
            value = value == Qt.CheckState.Checked

        change = False
        if item.is_label and index.column() == 0:
            item.content = str(value)
            change = True
        elif item.is_flag and index.column() == 0:
            item.content = bool(value)
            change = True
        elif self.has_mapped_properties():
            change = item.set_property(self.property_name(index), value)

        if change:
            self.dataChanged.emit(index, index, [role])

        return change

    # overload QAbstractItemModel.supportedDragActions()
    def supportedDragActions(self) -> Qt.DropAction:
        """Returns the actions supported by the data in this model."""
        return Qt.DropAction.MoveAction

    # overload QAbstractItemModel.supportedDropActions()
    def supportedDropActions(self) -> Qt.DropAction:
        """Returns the drop actions supported by this model."""
        return Qt.DropAction.MoveAction

    # overload QAbstractItemModel.mimeTpyes()
    def mimeTypes(self) -> List[str]:
        """Returns the list of allowed MIME types."""
        return ["application/x-qtreeitem"]

    # overload QAbstractItemModel.dropMimeData()
    def dropMimeData(
        self,
        data: QMimeData,
        action: Qt.DropAction,
        row: int,
        column: int,
        parent: QModelIndex,
    ) -> bool:

        if data.hasFormat("application/x-qtreeitem"):
            if hasattr(data, "x_qtreeitems"):
                return self.moveItems(data.x_qtreeitems, parent, row)
            else:
                return False
        else:
            return False

    # overload QAbstractItemModel.mimeData()
    def mimeData(self, indexes: List[QModelIndex]) -> QMimeData:
        """Returns an object that contains serialized items of data corresponding to the list of indexes specified."""
        if not indexes:
            return None

        data = QMimeData()
        items = list(filter(None, [index.internalPointer() for index in indexes]))
        data.setData("application/x-qtreeitem", QByteArray())
        # FIXME: avoid cast to QByteArray ... in a dirty way
        assert not hasattr(data, "x_qtreeitems")
        data.__setattr__("x_qtreeitems", items)

        return data

    ##############

    def moveItems(
        self,
        sourceItems: List[qTreeItem],
        destinationParent: QModelIndex,
        destinationRow: int,
    ) -> bool:
        """move a list of assumed contiguous items.

        Args:
            sourceItems (List[qTreeItem]): list of items
            destinationParent (QModelIndex): index of the destination
            destinationChild (int): position in the destination

        Returns:
            bool: whether the move was a success or not
        """
        destination_item = self._child_item(destinationRow, destinationParent)
        if destination_item is None:
            assert destinationParent.isValid()
            destination_item = destinationParent.internalPointer()
        assert isinstance(destination_item, qTreeItem)
        if destinationRow < 0:
            destinationRow = destination_item.length

        # items are supposed contiguous BUT can have different parents
        # so we split the move into chunks by source parents,
        # each move is evaluated by Qt (cant move a parent in its child)

        # group moved rows by parents
        move_rows = defaultdict(list)
        for item in sourceItems:
            move_rows[item.parent].append(item.row_in_parent())

        # handle move parent per parent
        for parent, rows in move_rows.items():
            parent_index = (
                QModelIndex()
                if parent == self.root
                else self.createIndex(parent.row_in_parent(), 0, parent)
            )
            # ask Qt if the move is valid
            if not self.beginMoveRows(
                parent_index,
                min(rows),  # assert contiguous selection only !
                max(rows),  # assert contiguous selection only !
                destinationParent,
                destinationRow,
            ):
                return False
            # latest row removed first
            for row in sorted(rows, reverse=True):
                item = parent.children.pop(row)
                item.parent = destination_item
                destination_item.children.append(item)
                destinationRow += 1
            # end the move event
            self.endMoveRows()

        return True

    def insert(
        self,
        content: Any,
        *,
        row: int = None,
        parent=QModelIndex(),
        map_properties: bool = None,
        droppable: bool = None,
        editable: bool = True,
    ) -> QModelIndex:
        """Insert a single row in the child items of the parent specified, and set its item.data().

        Args:
            content (Any, optional): item.data() content. Defaults to None.
            row (int, optional): row position in the parent (should be 0 or -1). Defaults to -1.
            parent (QModelIndex, optional): parent model index. Defaults to `self.root`.
            dropable (bool, optional): boolean switch to force dropable flag. Defaults to None.

        Returns:
            qTreeItem: `content` qTreeItem representation
        """
        assert content is not None
        assert not isinstance(content, qTreeItem)
        if droppable is None:
            droppable = isinstance(content, str)
        if map_properties is None:
            map_properties = any(hasattr(content, name) for name in self.columns)

        if map_properties and not self.has_mapped_properties():
            raise ValueError

        parent_item = self._item(parent, self.root)

        target_row = row or self.rowCount()
        assert 0 <= target_row <= self.rowCount()

        new_item = qTreeItem(
            content,
            parent=parent_item,
            droppable=droppable,
            expose_properties=map_properties,
            editable=editable,
        )
        self.beginInsertRows(parent, target_row, target_row)
        # append is WAY faster than insert
        if row:
            parent_item.children.insert(row, new_item)
        else:
            parent_item.children.append(new_item)
        self.endInsertRows()

        return self.createIndex(target_row, 0, new_item)

    def fetch(self, content: Any) -> QModelIndex:
        """find an item by content and return its index

        Args:
            content (Any): the object to find.

        Returns:
            QModelIndex: its index in the model (can be invalid).
        """

        for item in self.iterator():
            if item == content:
                return self.createIndex(item.row_in_parent(), 0, item)
        return QModelIndex()

    def pop(self, index: Any | QModelIndex) -> qTreeItem:
        """remove an item from the model and return it

        Args:
            index (Any | QModelIndex): index (or content) to remove.

        Returns:
            qTreeItem: removed item, can be None.
        """
        if not isinstance(index, QModelIndex):
            index = self.fetch(index)

        if not index.isValid():
            return None

        item = index.internalPointer()
        if item is None:
            return None

        parent = item.parent
        if parent is None:
            return None

        row = item.row_in_parent()
        self.beginRemoveRows(index, row, row)
        old = parent.children.pop(row)
        self.endRemoveRows()

        return old
