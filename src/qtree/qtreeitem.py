# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import Any

from qtpy.QtCore import Qt


class qTreeItem:
    def __init__(
        self,
        content: Any = None,
        *,
        parent: qTreeItem = None,
        droppable: bool = False,
        expose_properties: bool = False,
        editable: bool = False,
    ):
        self.content = content
        self.parent = parent
        self.children = []
        self.expose_properties = expose_properties
        self._flags = (
            Qt.ItemFlag.ItemIsEnabled
            | Qt.ItemFlag.ItemIsSelectable
            | Qt.ItemFlag.ItemIsDragEnabled
        )
        if editable:
            self._flags |= Qt.ItemFlag.ItemIsEditable
        if droppable:
            self._flags |= Qt.ItemFlag.ItemIsDropEnabled

    def __repr__(self) -> str:
        """instance representation"""
        return f"<'{self.__class__.__name__}' {self.content}>"

    def __str__(self) -> str:
        """instance string conversion"""
        return f"{self.content}"

    def __eq__(self, other):
        """allow comparison with item content"""
        if type(other) == type(self):
            return super().__eq__(other)
        else:
            return self.content == other

    def __hash__(self) -> int:
        """make item hash unique"""
        return id(self)

    @property
    def flags(self):
        return self._flags

    @property
    def editable(self):
        return bool(self._flags & Qt.ItemFlag.ItemIsEditable)

    @property
    def droppable(self):
        return bool(self._flags & Qt.ItemFlag.ItemIsDropEnabled)

    @property
    def length(self):
        """number of direct children"""
        return len(self.children)

    @property
    def size(self):
        """total number of items"""
        return 1 + sum(1 for _ in self.iterator())

    def row_in_parent(self):
        """position of item in parent children"""
        if not self.parent or self not in self.parent.children:
            # FIXME: throw ??
            return -1  # invalid
        return self.parent.children.index(self)

    def iterator(self):
        """recursive iterator on children"""
        children = self.children[::-1]
        while children:
            child = children.pop()
            if child.children:
                children += child.children[::-1]
            yield child

    def child(self, row: int):
        try:
            return self.children[row]
        except IndexError:
            return None

    def get_property(self, attr: str, default: Any = None) -> Any:
        """get content attribute"""
        return getattr(self.content, attr, default)

    def set_property(self, attr: str, value: Any) -> bool:
        """set content attribute"""
        if hasattr(self.content, attr):
            setattr(self.content, attr, value)
            return True
