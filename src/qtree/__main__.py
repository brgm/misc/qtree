#!/usr/bin/python3
# -*- coding: utf-8 -*-

import string
import sys

from qtpy.QtCore import Qt
from qtpy.QtWidgets import QApplication, QFrame, QLabel, QMainWindow, QSplitter

from .qtreemodel import qTreeModel
from .qtreeview import qTreeView

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = QMainWindow()
    window.setWindowTitle("QTree")

    splitter = QSplitter(Qt.Horizontal, parent=window)
    splitter.setChildrenCollapsible(False)
    window.setCentralWidget(splitter)

    tree = qTreeView()
    tree.setFrameStyle(QFrame.Plain)
    # tree.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Ignored) # FIXME
    label = QLabel()
    label.setFrameStyle(QFrame.Plain)
    label.setAlignment(Qt.AlignCenter)
    # label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Ignored) # FIXME

    splitter.addWidget(tree)
    splitter.addWidget(label)

    model = qTreeModel()
    tree.setModel(model)

    model.insert("first")
    model.insert("second")
    model.insert("third")

    parent = model.insert("letters")
    for _ in string.ascii_letters[:4]:
        model.insert(_, parent=parent)

    parent = model.insert("numbers")
    for _ in range(4):
        model.insert(_, parent=parent)

    def update_label():
        current = tree.currentIndex()
        item = current.internalPointer()
        msg = str(item) if current.isValid() else "no selection"
        label.setText(msg)

    selectModel = tree.selectionModel()
    selectModel.currentChanged.connect(update_label)
    model.dataChanged.connect(update_label)

    window.show()

    sys.exit(app.exec())
