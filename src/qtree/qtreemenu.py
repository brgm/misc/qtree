# -*- coding: utf-8 -*-
from __future__ import annotations

from typing import Callable

from qtpy.QtCore import QModelIndex
from qtpy.QtGui import QIcon
from qtpy.QtWidgets import QAbstractItemView, QAction, QMenu

from .qtreemodel import qTreeModel


class qTreeMenu(QMenu):
    def __init__(self, parent: QAbstractItemView, index: QModelIndex = None):
        if not isinstance(parent, QAbstractItemView):
            raise TypeError(parent)
        if not isinstance(parent.model(), qTreeModel):
            raise TypeError(parent.model())
        super().__init__("Context menu", parent=parent)

        index = index or parent.currentIndex()
        self.item = index.internalPointer() or self.model.root

        self.add_action(
            name="Add item",
            callback=lambda: self.model.insert(
                f"New item",
                parent=index,
                row=index.row(),
            ),
        )
        if self.item != self.model.root:
            self.addSeparator()
            self.add_action(
                name="Delete item", callback=lambda: self.model.pop(self.item)
            )

    @property
    def view(self):
        return self.parent()

    @property
    def model(self):
        return self.view.model()

    @property
    def index(self):
        return self.model.createIndex(self.item.row, 0, self.item)

    def add_action(
        self,
        name: str,
        callback: Callable,
        icon: str = None,
        shortcut=None,
        enable: bool = True,
    ):
        if icon:
            action = QAction(QIcon(icon), name, parent=self)
        else:
            action = QAction(name, parent=self)
        if shortcut:
            action.setShortcut(shortcut)
        action.triggered.connect(callback)
        action.setEnabled(enable)
        self.addAction(action)
