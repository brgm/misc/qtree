# -*- coding: utf-8 -*-
from __future__ import annotations

from qtpy.QtCore import QObject, Signal


class qObject(QObject):

    attributeChanged = Signal(str)

    def __init__(self):
        super().__init__()

    def __setattr__(self, __name: str, __value: Any) -> None:
        super().__setattr__(__name, __value)
        if not __name.startswith("_"):
            self.attributeChanged.emit(__name)
