# -*- coding: utf-8 -*-
from __future__ import annotations

from qtpy.QtCore import QModelIndex, QPoint, Qt
from qtpy.QtWidgets import QAbstractItemView, QTreeView, QWidget

from .qtreemenu import qTreeMenu
from .qtreemodel import qTreeModel


class qTreeView(QTreeView):
    """QTreeView initialized with a qTreeModel.

    Defines drag'n'drop default behavior to internal move.

    Args:
        parent (QWidget, optional): parent QWidget. Defaults to None.
        columns (List[str], optional): list of `item.data()` attributes to expose. Defaults to [].
        header (bool, optional): show model header (exposed attributes names). Defaults to False.
    """

    def __init__(self, parent: QWidget = None, *, header: bool = False) -> None:
        super().__init__(parent)

        self.setHeaderHidden(not header)

        self.setAcceptDrops(True)
        self.setDragEnabled(True)
        self.setSelectionMode(QAbstractItemView.ContiguousSelection)
        self.setDragDropMode(QAbstractItemView.InternalMove)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.on_context_menu)

    @property
    def current(self) -> QModelIndex:
        return self.view.currentIndex()

    def setModel(self, model: qTreeModel) -> None:
        if not isinstance(model, qTreeModel):
            raise TypeError(model)
        return super().setModel(model)

    def on_context_menu(self, point: QPoint = None):
        index = self.indexAt(point) if point else self.current
        menu = qTreeMenu(parent=self, index=index)
        menu.exec(self.viewport().mapToGlobal(point))
