# -*- coding: utf-8 -*-

from dataclasses import dataclass

import pytest

from qtree import qTreeModel
from qtree.qtreemodel import Qt


@dataclass
class MockA:
    propA = 1


@dataclass
class MockB:
    propB = 1


@dataclass
class MockAB:
    propA = "A"
    propB = "B"


@dataclass
class MockC:
    propC = 1


flag = True
integer = 42
label = "foo"
a = MockA()
b = MockB()
c = MockC()
ab = MockAB()


def test_unfiltered_model():
    model = qTreeModel()
    model_data = lambda i, j: model.data(model.index(i, j))
    assert not model.has_mapped_properties()
    with pytest.raises(ValueError):
        model.insert(a, map_properties=True)
    model.insert(a)
    assert model_data(0, 0) == a
    assert model_data(0, 1) is None
    model.insert(label)
    assert model_data(0, 0) == a
    assert model_data(0, 1) is None
    assert model_data(1, 0) == label
    assert model_data(1, 1) is None
    model.insert(flag)
    assert model_data(0, 0) == a
    assert model_data(1, 0) == label
    assert isinstance(model_data(2, 0), bool)
    assert model_data(2, 0) == flag


def test_filtered_model():
    model = qTreeModel(columns=["propA", "propB"])
    model_data = lambda i, j: model.data(model.index(i, j))
    model_flags = lambda i, j: model.flags(model.index(i, j))
    assert model.has_mapped_properties()
    index = model.insert(a)
    assert model.has_mapped_properties(0)
    assert model.data(index) == a.propA
    assert model_data(0, 0) == a.propA
    assert model_data(0, 1) is None
    index = model.insert(a, map_properties=False)
    assert not model.has_mapped_properties(1)
    assert model.data(index) == a
    assert model_data(1, 0) == a
    assert model_data(1, 1) is None
    model.insert(b)
    assert model.has_mapped_properties(2)
    assert model_data(2, 0) is None
    assert model_data(2, 1) == b.propB
    model.insert(b, map_properties=False)
    assert not model.has_mapped_properties(3)
    assert model_data(3, 0) == b
    assert model_data(3, 1) is None
    model.insert(c)
    assert model.rowCount() == 5
    assert not model.has_mapped_properties(4)
    assert model_data(4, 0) == c
    assert model_data(4, 1) is None
    model.insert(c, map_properties=True)
    assert model.rowCount() == 6
    assert model.has_mapped_properties(5)
    assert model_data(5, 0) is None
    assert model_data(5, 1) is None
    assert model.rowCount() == 6
    for i in range(model.rowCount()):
        for j in range(2):
            assert not (model_flags(i, j) & Qt.ItemFlag.ItemIsDropEnabled)
            assert model_data(i, j) is None or (
                model_flags(i, j) & Qt.ItemFlag.ItemIsEditable
            )
    model.insert(a, editable=False)
    model.insert(label)
    assert model.rowCount() == 8
    assert not model.has_mapped_properties(7)
    assert model_data(7, 0) == label
    assert model_data(7, 1) is None
    assert model_flags(7, 0) & Qt.ItemFlag.ItemIsDropEnabled


def test_insert_generations():
    model = qTreeModel()
    index = model.insert("grand-parent")
    index = model.insert("parent", parent=index)
    model.insert("son", parent=index)


if __name__ == "__main__":
    test_item_creation()
    test_unfiltered_model()
    test_filtered_model()
