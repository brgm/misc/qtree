# -*- coding: utf-8 -*-
from sys import platform

import pytest
from qtpy.QtCore import QModelIndex, Qt

from qtree import qTreeModel, qTreeView

pytestmark = pytest.mark.skipif(platform == "linux", reason="only works on Windows :-/")


def test_view(qtbot):
    view = qTreeView()
    qtbot.addWidget(view)


def test_clicks_on_item(qtbot):
    view = qTreeView()
    model = qTreeModel()
    view.setModel(model)
    qtbot.addWidget(view)

    # left/right click outside items
    point = view.visualRect(QModelIndex()).center()
    qtbot.mouseClick(view, Qt.LeftButton, pos=point)
    qtbot.mouseClick(view, Qt.RightButton, pos=point)

    # left/right click on an item
    first = model.insert("first")
    point = view.visualRect(model.fetch(first)).center()
    qtbot.mouseClick(view, Qt.LeftButton, pos=point)
    qtbot.mouseClick(view, Qt.RightButton, pos=point)
