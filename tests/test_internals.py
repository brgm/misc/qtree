# -*- coding: utf-8 -*-

import itertools

from qtree.qtreeitem import qTreeItem


def test_item_creation():
    for droppable, expose_properties, editable in itertools.product(
        (False, True), repeat=3
    ):
        item = qTreeItem(
            42,
            droppable=droppable,
            expose_properties=expose_properties,
            editable=editable,
        )
        assert item.droppable == droppable
        assert item.expose_properties == expose_properties
        assert item.editable == editable
