# -*- coding: utf-8 -*-

import string

from qtree import qTreeModel


def test_model(qtmodeltester):
    model = qTreeModel()
    qtmodeltester.check(model)


def test_simple_group_insertion(qtmodeltester):
    model = qTreeModel()
    group = model.insert("level 0")
    model.insert("level 1", parent=group)
    qtmodeltester.check(model)


def test_simple_group_insertions(qtmodeltester):
    n = 4
    model = qTreeModel()
    group = model.insert("level 0")
    for _ in string.ascii_letters[:n]:
        model.insert(_, parent=group)
    assert model.rowCount() == 1
    assert model.rowCount(group) == n
    qtmodeltester.check(model)


def test_model_insertion(qtmodeltester):
    model = qTreeModel()
    assert model.size() == 1
    assert model.length == 0

    # test single object 'raw' insertion
    model.insert("first")
    assert model.size() == 2
    assert model.length == 1

    # test single object 'wrapped' insertion
    model.insert("second")
    assert model.size() == 3
    assert model.length == 2

    # test single object 'soft' insertion
    model.insert("third")
    assert model.size() == 4
    assert model.length == 3

    # test nested objects 'wrapped' insertion
    group = model.insert("letters")
    for _ in string.ascii_letters[:4]:
        model.insert(_, parent=group)
    assert model.size() == 9
    assert model.length == 4

    # test nested objects 'wrapped' insertion
    group = model.insert("numbers")
    for _ in range(4):
        model.insert(_, parent=group)
    assert model.size() == 14
    assert model.length == 5

    qtmodeltester.check(model)


def test_fetch(qtmodeltester):
    model = qTreeModel()

    # fetch content
    model.insert("first")
    fetch = model.fetch("first")
    assert fetch.isValid()
    assert fetch.internalPointer() == "first"

    # fetch item
    model.insert("second")
    fetch = model.fetch("second")
    assert fetch.isValid()
    assert fetch.internalPointer() == "second"


def test_model_removal(qtmodeltester):
    model = qTreeModel()

    item = model.pop("fail")
    assert item is None

    # remove content
    model.insert("first")
    assert model.size() == 2
    assert model.length == 1
    item = model.pop("first")
    assert model.size() == 1
    assert model.length == 0
    assert item == "first"

    # remove item
    model.insert("second")
    assert model.size() == 2
    assert model.length == 1
    item = model.pop("second")
    assert model.size() == 1
    assert model.length == 0
    assert item == "second"

    group = model.insert("numbers")
    for _ in range(4):
        model.insert(_, parent=group)
    assert model.size() == 6
    assert model.length == 1
    model.pop(2)
    assert model.size() == 5
    assert model.length == 1
    item = model.pop("numbers")
    assert model.size() == 1
    assert model.length == 0
    assert item == "numbers"

    qtmodeltester.check(model)
