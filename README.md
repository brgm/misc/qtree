# QTree

[Qt] [model]/[view] to display a collection of drag'n'dropable objects.  

## Overview

QTree's implementation relies on [qtpy] abstraction layer to provide PyQt5/PySide2/PyQt6/PySide6 support.  

It ships:
- __[`qTreeModel`](src/qtree/qtreemodel.py):__ an implementation of `QAbstractItemModel` that accepts any `object` as `item.data()`, exposing the attributes declared in the `columns: List[str]` argument in the constructor as model columns *(it defaults to the `str(object)` representation of the object if `columns=[]`)*.  
It relies on :
    -  __`qTreeModel.insert(self, content: Any = None, **kwargs)`:__ to add an item
    -  __`qTreeModel.pop(self, index: Any | QModelIndex)`:__ to remove an item
- __[`qTreeView`](src/qtree/qtreeview.py):__ an implementation of `QTreeView` that accepts *(and initialize)* only `qTreeModel` instances as `setModel()` argument, setting up `InternalMove` and `ContiguousSelection` as default behaviors.

## Usage

QTree [example](src/qtree/__main__.py) can be run by calling `python -m qtree` *(requires to install the package)*.

## Installation

QTree should be pushed to the [BRGM nexus] as soon as production ready, which would allow a simple:
```sh
pip install qtree -i https://nexus.brgm.fr/repository/pypi-all/simple
```
Meanwhile one can build it manually from gitlab:
```bash
pip install git+https://gitlab.brgm.fr/brgm/geomodelling/qt/qtree
```
To locally build/install this package, use:
```sh
git clone https://gitlab.brgm.fr/brgm/geomodelling/qt/qtree
cd qtree
# regular install
python -m pip install .
# install as editable
python -m pip install -e .
# or package wheel
python -m pip wheel . -w ./dist --no-deps
```

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## License
QTree is provided under the GNU General Public License (GPLv3) that can be found in the [LICENSE](LICENSE) file.
By using, distributing, or contributing to this project, you agree to the terms and conditions of this license.

[BRGM nexus]:https://nexus.brgm.fr/
[Qt]:https://www.qt.io/
[qtpy]:https://github.com/spyder-ide/qtpy
[model]:https://doc.qt.io/qt-6/qabstractitemmodel.html
[view]:https://doc.qt.io/qt-6/qtreeview.html
